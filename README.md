CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers


INTRODUCTION
------------

The Add to Cart Pop-up Form module provides an add to cart link pseudo
field to each product type that allows you to display the product,
selected variation, and add to cart form in a themeable pop-up modal when
the link is clicked. When the pop-up form is submitted, the cart block count
is automatically updated and a themeable success form is displayed.

The pop-up modal allows you to configure the height and width of the modal
in the manage display section for each product type. The pop-up modal is
populated by fields configured in custom Add To Cart Pop-Up display modes for
the Product Type, Variation Type, and Order Item Type.

REQUIREMENTS
------------

This module requires the cart submodule from drupal commerce.


INSTALLATION
------------

Install the Add to Cart Pop-up Form module as you would normally install a contributed
Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.


CONFIGURATION
-------------

    1. Go to Manage Display for your Product Type and Variation Type and enable
       the Add To Cart Pop-Up display mode.
    2. Set the weight of each field you wish to display in the pop-up form. Ensure
       that the Variations field is set to the Add To Cart formatter. Just like building
       a product page, the variation fields can be injected into the pop-up form to display
       alongside the product fields.
    3. Edit the Product Type display mode you wish to display the Add To Cart Pop-Up Link
       and drag the Add to cart pop-up form field into the visible area. Set the width and
       height of the modal form and choose whether you wish to combine similar order items.
    4. Go to Manage Form Display for your Order Item Type and enable the Add To Cart
       Pop-Up form display mode. Set the Purchased Entity field to either the Product variation
       title or attributes widget, and hide the Unit Price field.


MAINTAINERS
-----------

* Eric Chew - https://www.drupal.org/u/ericchew
