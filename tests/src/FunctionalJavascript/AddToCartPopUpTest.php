<?php

namespace Drupal\Tests\commerce_add_to_cart_popup_form\FunctionalJavascript;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\commerce_product\Entity\ProductVariationType;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Tests\commerce_cart\FunctionalJavascript\CartWebDriverTestBase;

/**
 * Tests the add to cart pop-up form.
 *
 * @group commerce_add_to_cart_popup_form
 */
class AddToCartPopUpTest extends CartWebDriverTestBase {

  /**
   * The color attributes.
   *
   * @var \Drupal\commerce_product\Entity\ProductAttributeValueInterface[]
   */
  protected $colorAttributes;

  /**
   * The size attributes.
   *
   * @var \Drupal\commerce_product\Entity\ProductAttributeValueInterface[]
   */
  protected $sizeAttributes;

  /**
   * The product variations.
   *
   * @var \Drupal\commerce_product\Entity\ProductVariationInterface[]
   */
  protected $variations;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'commerce_add_to_cart_popup_form',
    'commerce_add_to_cart_popup_form_test',
  ];

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();

    /** @var \Drupal\commerce_product\Entity\ProductVariationTypeInterface $variation_type */
    $variation_type = ProductVariationType::load($this->variation->bundle());

    $this->colorAttributes = $this->createAttributeSet($variation_type, 'color', [
      'red' => 'Red',
      'blue' => 'Blue',
    ]);
    $this->sizeAttributes = $this->createAttributeSet($variation_type, 'size', [
      'small' => 'Small',
      'medium' => 'Medium',
      'large' => 'Large',
    ]);

    // Reload the variation since we have new fields.
    $this->variation = ProductVariation::load($this->variation->id());
    $product = $this->variation->getProduct();

    // Update first variation to have the attribute's value.
    $this->variation->attribute_color = $this->colorAttributes['red']->id();
    $this->variation->attribute_size = $this->sizeAttributes['small']->id();
    $this->variation->save();

    // The matrix is intentionally uneven, blue / large is missing.
    $attribute_values_matrix = [
      ['red', 'small'],
      ['red', 'medium'],
      ['red', 'large'],
      ['blue', 'small'],
      ['blue', 'medium'],
    ];
    $variations = [
      $this->variation,
    ];
    // Generate variations off of the attributes values matrix.
    foreach ($attribute_values_matrix as $key => $value) {
      $variation = $this->createEntity('commerce_product_variation', [
        'type' => $variation_type->id(),
        'sku' => $this->randomMachineName(),
        'price' => [
          'number' => 999,
          'currency_code' => 'USD',
        ],
        'attribute_color' => $this->colorAttributes[$value[0]],
        'attribute_size' => $this->sizeAttributes[$value[1]],
      ]);
      $variations[] = $variation;
      $product->variations->appendItem($variation);
    }
    $product->save();
    $this->variations = $variations;

    /** @var \Drupal\Core\Entity\Display\EntityViewDisplayInterface $product_default_display */
    $product_default_display = EntityViewDisplay::load('commerce_product.default.default');
    $product_default_display->setComponent('commerce_add_to_cart_popup_form', [
      'weight' => 2,
    ]);
    $product_default_display->setThirdPartySetting('commerce_add_to_cart_popup_form', 'field_settings', [
      'combine' => '1',
      'display' => [
        'width' => '800',
        'height' => '',
      ],
    ]);
    $product_default_display->save();

    $product = $this->variation->getProduct();
    $product->body->value = 'Testing product body';
    $product->save();
  }

  /**
   * Tests the add to cart pop-up form.
   */
  public function testAddToCartPopUpForm() {
    $page = $this->getSession()->getPage();

    $product = $this->variation->getProduct();
    $this->drupalGet($product->toUrl());
    $page->clickLink('Add to cart');
    $this->assertSession()->assertWaitOnAjaxRequest();

    // Test the modal is visible.
    $this->assertSession()->elementExists('css', '#drupal-modal');

    // Test the modal contains the product's body text.
    $this->assertModalTextContains($product->body->value);

    // Test the product variation attributes are displayed.
    $this->assertModalAttributeSelected('purchased_entity[0][attributes][attribute_color]', 'Red');
    $this->assertModalAttributeSelected('purchased_entity[0][attributes][attribute_size]', 'Small');
    $this->assertModalAttributeExists('purchased_entity[0][attributes][attribute_color]', $this->colorAttributes['blue']->id());
    $this->assertModalAttributeExists('purchased_entity[0][attributes][attribute_size]', $this->sizeAttributes['medium']->id());
    $this->assertModalAttributeExists('purchased_entity[0][attributes][attribute_size]', $this->sizeAttributes['large']->id());

    // Use AJAX to change the size to Medium, keeping the color on Red.
    $this->getSession()->getPage()->findById('drupal-modal')->selectFieldOption('purchased_entity[0][attributes][attribute_size]', 'Medium');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->assertModalAttributeSelected('purchased_entity[0][attributes][attribute_color]', 'Red');
    $this->assertModalAttributeSelected('purchased_entity[0][attributes][attribute_size]', 'Medium');
    $this->assertModalAttributeExists('purchased_entity[0][attributes][attribute_color]', $this->colorAttributes['blue']->id());
    $this->assertModalAttributeExists('purchased_entity[0][attributes][attribute_size]', $this->sizeAttributes['small']->id());
    $this->assertModalAttributeExists('purchased_entity[0][attributes][attribute_size]', $this->sizeAttributes['large']->id());

    $selected_variation = $this->variations[2];

    // Test ajax field replacement.
    $this->assertModalTextContains($selected_variation->label());

    // Test errors are displayed.
    $this->getSession()->getPage()->findById('drupal-modal')->findField('quantity[0][value]')->setValue(0);
    $this->getModalButtons()->pressButton('Add to cart');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->assertModalTextContains('Quantity must be higher than or equal to 1.');

    // Submit the form without errors.
    $this->getSession()->getPage()->findById('drupal-modal')->findField('quantity[0][value]')->setValue(1);
    // Submit the add to cart form.
    $this->getModalButtons()->pressButton('Add to cart');
    $this->assertSession()->assertWaitOnAjaxRequest();

    // Test the success form contains the add to cart message.
    $this->assertModalTextContains($selected_variation->label() . ' added to your cart.');
    // Test the modal has the correct buttons.
    $this->assertTrue($this->getModalButtons()->hasButton('Continue shopping'));
    $this->assertTrue($this->getModalButtons()->hasButton('View cart'));

    // Test the variation exists in the cart.
    $this->cart = Order::load($this->cart->id());
    $order_items = $this->cart->getItems();
    $this->assertOrderItemInOrder($this->variations[2], $order_items[0]);
  }

  /**
   * Asserts that the modal contains the given text.
   *
   * @param string $text
   *   The text to search for.
   */
  protected function assertModalTextContains(string $text) {
    $this->assertSession()->elementTextContains('css', '#drupal-modal', $text);
  }

  /**
   * Gets the modal element.
   */
  protected function getModal() {
    return $this->getSession()->getPage()->findById('drupal-modal');
  }

  /**
   * Gets the modal buttons element.
   */
  protected function getModalButtons() {
    return $this->getSession()->getPage()->find('css', '.ui-dialog-buttonset');
  }

  /**
   * Asserts that an attribute option is selected in the modal.
   *
   * @param string $selector
   *   The element selector.
   * @param string $option
   *   The option.
   */
  protected function assertModalAttributeSelected($selector, $option) {
    $selected_option = $this->getSession()->getPage()->findById('drupal-modal')->find('css', 'select[name="' . $selector . '"] option[selected="selected"]')->getText();
    $this->assertEquals($option, $selected_option);
  }

  /**
   * Asserts that an attribute option does exist in the modal.
   *
   * @param string $selector
   *   The element selector.
   * @param string $option
   *   The option.
   */
  protected function assertModalAttributeExists($selector, $option) {
    $this->assertSession()->elementExists('xpath', '//div[@id="drupal-modal"]//select[@name="' . $selector . '"]//option[@value="' . $option . '"]');
  }

}
